# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=gitui
pkgver=0.22.0
pkgrel=0
pkgdesc="Blazing fast terminal client for git"
url="https://github.com/extrawurst/gitui"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="MIT"
makedepends="
	cargo
	libgit2-dev
	oniguruma-dev
	openssl-dev
	zlib-dev
	"
source="https://github.com/extrawurst/gitui/archive/v$pkgver/gitui-$pkgver.tar.gz"

export LIBSSH2_SYS_USE_PKG_CONFIG=1  # use system libssh2
export RUSTONIG_DYNAMIC_LIBONIG=1  # use system libonig

_cargo_opts="--frozen --no-default-features --features regex-onig"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build $_cargo_opts --release
}

check() {
	cargo test $_cargo_opts
}

package() {
	install -D -m755 target/release/gitui "$pkgdir"/usr/bin/gitui
}

sha512sums="
2ef5ca02f05dcf3adb2699f85e70b4cc5816f3defd228524a4563a055faaf02d34ef4d46e682f6b43d12b0b74ec674ace26076083ee0bf028e4ce7b664801544  gitui-0.22.0.tar.gz
"
