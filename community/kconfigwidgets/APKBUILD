# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kconfigwidgets
pkgver=5.100.0
pkgrel=0
pkgdesc="Widgets for KConfig"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kauth-dev
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	kguiaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfigwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kstandardactiontest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0fa097fdcb9e84b7688a95b9a7d7ab4b11b6f12ea335ff187a405ba0996a6325a66927cd34adadcc9b755798ddf99baef309ac378c7f4296e4b6a7fedfb12084  kconfigwidgets-5.100.0.tar.xz
"
