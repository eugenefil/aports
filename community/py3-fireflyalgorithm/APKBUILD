# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-fireflyalgorithm
_pkgorig=FireflyAlgorithm
pkgver=0.3.3
pkgrel=1
pkgdesc="An implementation of the stochastic nature-inspired algorithm for optimization"
url="https://github.com/firefly-cpp/FireflyAlgorithm"
arch="noarch"
license="MIT"
depends="python3 py3-numpy"
makedepends="py3-gpep517 py3-poetry-core"
checkdepends="py3-pytest"
source="https://github.com/firefly-cpp/FireflyAlgorithm/archive/$pkgver/$_pkgorig-$pkgver.tar.gz"
builddir="$srcdir/FireflyAlgorithm-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/fireflyalgorithm-$pkgver-py3-none-any.whl
}

sha512sums="
a5466b52f22ec599b8afce0e2e5ea4da0d9eaf959b00443b656ac3666cec5f869180cdd4b59dcedc64bc4a669092076ee8ab6e7fe82b37de255161583362cfc1  FireflyAlgorithm-0.3.3.tar.gz
"
