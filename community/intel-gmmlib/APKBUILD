# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=intel-gmmlib
pkgver=22.3.1
pkgrel=0
pkgdesc="Intel Graphics Memory Management Library"
options="!check" # tests can't run in check(), only on install
url="https://github.com/intel/gmmlib/"
arch="aarch64 x86_64 x86"
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/intel/gmmlib/archive/intel-gmmlib-$pkgver.tar.gz"
builddir="$srcdir/gmmlib-intel-gmmlib-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		-DRUN_TEST_SUITE=OFF \
		$CMAKE_CROSSOPTS
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6c2bb55f9c8762a2f8f279d98d08f63a97cd4755186d10ad6ad1f364002eb913f80bc7ea5fb1f9c6eaf85b03c7c60c16c9e636c3827483c51e2914e6e6e18e94  intel-gmmlib-22.3.1.tar.gz
"
