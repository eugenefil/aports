# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-pylint
pkgver=2.15.7
pkgrel=0
pkgdesc="Analyzes Python code looking for bugs and signs of poor quality"
url="https://github.com/PyCQA/pylint"
arch="noarch !s390x" # py3-dill
license="GPL-2.0-or-later"
depends="
	py3-astroid
	py3-dill
	py3-isort
	py3-mccabe
	py3-platformdirs
	py3-tomlkit
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-pytest
	py3-pytest-runner
	"
options="!check" # https://github.com/PyCQA/pylint/issues/3895
source="https://files.pythonhosted.org/packages/source/p/pylint/pylint-$pkgver.tar.gz
	"
builddir="$srcdir"/pylint-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	# Requires unpackaged 'pytest-benchmark'
	rm -rf tests/benchmark
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/pylint-*.whl
}

sha512sums="
98651b7d117fc0066b0b3c6f4a60cf0e768868e4e8bdee3b3107eb07ca92f5944806e818d9390ca910a128b0e5ff2e1461e263ed195d035ab474728e9c65ba7e  pylint-2.15.7.tar.gz
"
