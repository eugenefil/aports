# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kxmlgui
pkgver=5.100.0
pkgrel=0
pkgdesc="User configurable main windows"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	attica-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="
	mesa-dri-gallium
	xvfb-run
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kxmlgui-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kxmlgui_unittest, ktoolbar_unittest and ktooltiphelper_unittest are broken
	LC_ALL=C CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E '(kxmlgui|ktoolbar|ktooltiphelper)_unittest'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ec638c856e17df36424f661cc8da04afce376b0bc2cdf52209f1557fbe5de947a183bc03afb7247bd4bc8ab48ceefad01c21d084a6265b6f4fbc2cb239c91583  kxmlgui-5.100.0.tar.xz
"
